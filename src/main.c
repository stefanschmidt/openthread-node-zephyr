/*
 * Copyright (c) 2021 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Oniro Gateway Blueprint OpenThread node example application
 *
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(openthreadnode, LOG_LEVEL_INF);
#include <net/net_pkt.h>

#include <errno.h>
#include <zephyr.h>
#include <sys/printk.h>
#include <stdio.h>

#define APP_BANNER "Oniro Gateway Blueprint OpenThread node"

void main(void)
{
	struct net_if *iface;
	static char buf[17];
	char *idx = buf;

	iface = net_if_get_first_by_type(&NET_L2_GET_NAME(OPENTHREAD));
	for (int i = 0; i < net_if_get_link_addr(iface)->len; i++) {
		snprintf(idx, 3, "%02x", net_if_get_link_addr(iface)->addr[i]);
		idx += 2;
	}

	LOG_INF(APP_BANNER);
	LOG_INF("OpenThread commissioner QR code: https://dhrishi.github.io/connectedhomeip/qrcode.html?data=v=1%%26%%26eui=%s%%26%%26cc=J01NU5",buf );

}
