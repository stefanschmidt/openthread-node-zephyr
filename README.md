# Gateway Blueprint OpenThread Node Zephyr Application

[[_TOC_]]

## Overview

A Zephyr application demonstrating the basic configuration to join and
participate in an OpenThread mesh network created by the Gateway blueprint of
the Oniro Project.

### Requirements
- Supported boards:
  - Arduino Nano 33 BLE

## Building and flashing
### Building and flashing with Zephyr SDK
With the Zephyr SDK installed on your build machine using west for building and
flashing is the easiest option:

```
$ west build -p auto -b arduino_nano_33_ble . && west flash
```


## How to setup a static network setting between Linux and zephyr for development

For development and testing purpose it might be useful to configure a static
thread link between Linux and zephyr without the involvement of any
commissioning process. This setup will use a networkkey that is hardcoded into
the Zephyr firmware, which make this guide suitable for development purpose
only. Do *not* use this in production.

### Zephyr: building and flashing with static network config overlay

When using the overlay-static.conf overlay file in the build the
joiner/commissioner functionality is disabled and a static networkkey and
setting configured.

```
$ west build -b arduino_nano_33_ble -- -DCONF_FILE="prj.conf overlay-static.conf" .
$ west flash
```

### Linux: build and flash OT RCP on Zephyr onto Arduino Nano 33 BLE:

On the Linux side we need 3 steps to setup.

Build a OpenThread radio co-processor firmware for a second Arduino Nano (this
one will act as a Thread radio for the Linux host). This is building a sample
that lives within the main Zephyr tree.

```
$ west build -b arduino_nano_33_ble -- -DCONF_FILE="prj.conf overlay-rcp.conf overlay-usb-nrf-br.conf" -DDTC_OVERLAY_FILE="usb.overlay" -DCONFIG_OPENTHREAD_THREAD_VERSION_1_2=y samples/net/openthread/coprocessor/
$ west flash
```

### Build and run ot-daemon:

After flashing the ready we need to build the OpenThread daemon that runs on the
Linux host and uses the radio to interact with a Thread network.
When using the radio co-processor firmware on top of Zephyr we need to ensure that ot-daemon does handle USB reset properly. The compile flag -DOT_SPINEL_RESET_CONNECTION=ON, as well as, the runtime parameter &uart-reset is taking care of the reset handling.

But there is still the underlying problem that during after a USB reset the
/dev/ttyACM0 device will come back as /dev/ttyACM1 for example. So instead of
using the ttyACMx device name we will use a serial/by-id lookup device name.
When you first attach the Ardunio to the Linux host, after flashing the radio
co-processor firmware, check the name in /dev/serial/by-id/ and not it down for
later use. (It's the one that points to the current /dev/ttyACMx device and has
Zephyr in its name). This device name will stay stable over USB reset so we no
longer need to worry about device names changing underneath our daemon process.

```
git clone https://github.com/openthread/openthread.git
cd openthread
./bootstrap
./script/cmake-build posix -DOT_SPINEL_RESET_CONNECTION=ON -DOT_THREAD_VERSION=1.2 -DOT_DAEMON=ON
sudo build/posix/src/posix/ot-daemon -v 'spinel+hdlc+uart:///dev/serial/by-id/usb-ZEPHYR_OpenThread_CoProcessor_NRF_E96CD39E53C49869-if00?uart-baudrate=115200&uart-reset'
```

### Configure the static Thread network settings on Linux

th the radio connected and ot-daemon running we can start the configuration
script from another shell. It does use the ot-ctl commandline tool to configure
the static Thread network to match the zephyr version.
```
sudo ./static-openthread-configuration.sh
```

The first device started should act as the Thread network leader and the second
one joining as child and being promoted to a router over time. IPv6 connectivity
between the two devices is working now and opens the floor for transport and
application layer protocols to use it.
