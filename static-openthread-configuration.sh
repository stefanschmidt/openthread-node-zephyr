#!/bin/sh
# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

set -e

ot-ctl thread stop
ot-ctl ifconfig down
ot-ctl dataset clear
ot-ctl dataset init new
ot-ctl dataset panid 0x1357
ot-ctl dataset extpanid 11112222deadbeef
ot-ctl dataset networkname OniroThread
ot-ctl dataset channel 26
ot-ctl dataset networkkey 00112233445566778899aabbccddeeff
ot-ctl dataset commit active
ot-ctl ifconfig up
ot-ctl thread start
